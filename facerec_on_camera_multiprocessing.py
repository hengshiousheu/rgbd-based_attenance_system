import face_recognition
import cv2
from multiprocessing import Process, Manager, cpu_count
import time
import numpy as np
import threading
import imutils
from pylepton import Lepton
from imutils.video import VideoStream

# This is a little bit complicated (but fast) example of running face recognition on live video from your webcam.
# This example is using multiprocess.

# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.


# Get next worker's id
def next_id(current_id, worker_num):
	if current_id == worker_num:
		return 1
	else:
		return current_id + 1

# Get previous worker's id
def prev_id(current_id, worker_num):
	if current_id == 1:
		return worker_num
	else:
		return current_id - 1

def detect_faces_from_img(img_src = None, fc = None):
	"""
	:param gray_arr: np grayscale aray with the image
	:param fc: cv2.CascadeClassifier object for face classification
	"""

	face_cascade = cv2.CascadeClassifier('./models/haarcascade_frontalcatface_extended.xml')
	img_src = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)
	faces = face_cascade.detectMultiScale(
		img_src,# it should be GRAY
		scaleFactor=1.1,
		minNeighbors=5,
		minSize=(30, 30),
		flags = cv2.CASCADE_SCALE_IMAGE
	)

	return faces

def ktoc(val):
	# 16-bit Kelvin to deg Celsius
	return (val - 27315) / 100.0

def raw_to_8bit(data):
	"""
	:Desc: # normalize to 0-255 # convert to uint8
	:param data: 
	:return:
	"""
	cv2.normalize(data, data, 0, 255, cv2.NORM_MINMAX)
	return np.uint8(data)

# A subprocess user to capture IR frames.
def lepton_capture(read_frame_list, Global, worker_num):
	device = "/dev/spidev0.0"
	with Lepton(device) as l:
		while not Global.is_exit:
			# If it's time to read a frame
			if Global.buff_num != next_id(Global.read_num, worker_num):
				data, _ = l.capture()
				#Another implementation way to capture frame
				resized_data = cv2.resize(ktoc(data[:,:]), (320, 240))
				frame = raw_to_8bit(resized_data)
				# Grab a single frame of video
				read_frame_list[Global.buff_num] = frame
				Global.buff_num = next_id(Global.buff_num, worker_num)
			else:
				time.sleep(0.01)

# A subprocess use to capture frames.
def capture(rgb_read_frame_list, Global, worker_num):
	
	while not Global.is_exit:
		# If it's time to read a frame
		if Global.buff_num != next_id(Global.read_num, worker_num):
			#Another implementation way to capture frame
			frame = vs.read()
			# Grab a single frame of video
			rgb_read_frame_list[Global.buff_num] = frame
			Global.buff_num = next_id(Global.buff_num, worker_num)
		else:
			time.sleep(0.01)

	# Release webcam
	vs.stop()

# A subprocess use to capture frames within RGB and IR.
def hyprid_capture(rgb_read_frame_list, read_frame_list, Global, worker_num):
	device = "/dev/spidev0.0"
	with Lepton(device) as l:
		while not Global.is_exit:
			# If it's time to read a frame
			if Global.buff_num != next_id(Global.read_num, worker_num):
				# Grab a single frame of IR video
				data, _ = l.capture()
				frame = ktoc(data[:,:])
				read_frame_list[Global.buff_num] = frame

				# Grab a single frame of RGB video
				rgb_frame = vs.read()
				rgb_read_frame_list[Global.buff_num] = rgb_frame
				Global.buff_num = next_id(Global.buff_num, worker_num)
			else:
				time.sleep(0.01)

		# Release webcam
		vs.stop()

def attach_temp_on_frame(frame_process, lepton_frame_process, face_locations):
	for (top, right, bottom, left) in face_locations:
		box = np.array([top, right, bottom, left])
		(top, right, bottom, left) = box.astype("int")
		roi = lepton_frame_process[top:bottom, left:right]
		temp = round(np.nanmean(roi[roi > 35]), 2)
		cv2.putText(frame_process, str(temp), (left + 6, bottom - 18), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1)
	
	return frame_process

def attach_names_on_frame(frame_process, face_locations, known_face_encodings, known_face_names):
	# Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
	#rgb_frame = frame_process[:, :, ::-1]
	rgb_frame = frame_process

	# Find all the faces and face encodings in the frame of video, cost most time
	face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

	# Loop through each face in this frame of video
	for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
		# See if the face is a match for the known face(s)
		matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

		name = "Unknown"

		# If a match was found in known_face_encodings, just use the first one.
		if True in matches:
			first_match_index = matches.index(True)
			name = known_face_names[first_match_index]

		# Draw a box around the face
		cv2.rectangle(frame_process, (left, top), (right, bottom), (0, 0, 255), 2)

		# Draw a label with a name below the face
		cv2.rectangle(frame_process, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
		font = cv2.FONT_HERSHEY_DUPLEX
		cv2.putText(frame_process, name, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)

	return frame_process

# Many subprocess use to process frames.
def process(worker_id, rgb_read_frame_list, read_frame_list, rgb_write_frame_list, write_frame_list, Global, worker_num):
	known_face_encodings = Global.known_face_encodings
	known_face_names = Global.known_face_names
	while not Global.is_exit:

		# Wait to read
		while Global.read_num != worker_id or Global.read_num != prev_id(Global.buff_num, worker_num):
			# If the user has requested to end the app, then stop waiting for webcam frames
			if Global.is_exit:
				break

			time.sleep(0.01)

		# Delay to make the video look smoother
		time.sleep(Global.frame_delay)

		# Read a single frame from frame list
		lepton_frame_process = read_frame_list[worker_id]
		frame_process = rgb_read_frame_list[worker_id]

		# Resize frame to the same windows size
		lepton_frame_process = cv2.resize(lepton_frame_process, Global.frameSize)
		frame_process = cv2.resize(frame_process, Global.frameSize)

		# Expect next worker to read frame
		Global.read_num = next_id(Global.read_num, worker_num)

		face_locations = face_recognition.face_locations(frame_process)
		if face_locations: 
			## Find all the faces and face encodings in the frame of video, cost most time
			frame_process = attach_names_on_frame(frame_process, face_locations, known_face_encodings, known_face_names)
			# If find faces in frame, we assign name and temp on the image.
			frame_process = attach_temp_on_frame(frame_process, lepton_frame_process, face_locations)
		else :
			pass
		
		# Wait to write
		while Global.write_num != worker_id:
			time.sleep(0.01)

		# Send frame to global
		rgb_write_frame_list[worker_id] = frame_process
		write_frame_list[worker_id] = raw_to_8bit(lepton_frame_process)

		# Expect next worker to write frame
		Global.write_num = next_id(Global.write_num, worker_num)


if __name__ == '__main__':

	# Global variables
	Global = Manager().Namespace()
	Global.buff_num = 1
	Global.read_num = 1
	Global.write_num = 1
	Global.frame_delay = 0
	Global.is_exit = False
	Global.frameSize = (320, 240)
	read_frame_list = Manager().dict()
	write_frame_list = Manager().dict()

	rgb_read_frame_list = Manager().dict()
	rgb_write_frame_list = Manager().dict()

	# Number of workers (subprocess use to process frames)
	if cpu_count() > 2:
		worker_num = cpu_count() - 1  # 1 for capturing frames
	else:
		worker_num = 2

	# Subprocess list
	p = []

	print("Init camera configuration.")
	# Are we using the Pi Camera?
	usingPiCamera = True
	# Set initial frame size.
	frameSize = (320, 240)
	# Initialize mutithreading the video stream.
	vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,
			framerate=24).start()
	# Allow the camera to warm up.
	time.sleep(2.0)

	# Create a thread to capture frames (if uses subprocess, it will crash on Mac)
	p.append(threading.Thread(target=hyprid_capture, args=(rgb_read_frame_list, read_frame_list, Global, worker_num,)))
	p[0].start()

	# Load a sample picture and learn how to recognize it.
	print("Getting pre-trained embedding(128D)")
	obama_image = face_recognition.load_image_file("./assets/hengxiu_800600.jpg")
	obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

	# Create arrays of known face encodings and their names
	Global.known_face_encodings = [
		obama_face_encoding,
	]
	Global.known_face_names = [
		"Heng-Xiu"
	]

	# Create workers
	for worker_id in range(1, worker_num + 1):
		p.append(Process(target=process, args=(worker_id, rgb_read_frame_list, read_frame_list, rgb_write_frame_list, write_frame_list, Global, worker_num,)))
		p[worker_id].start()

	# Start to show video
	last_num = 1
	fps_list = []
	tmp_time = time.time()
	while not Global.is_exit:
		while Global.write_num != last_num:
			last_num = int(Global.write_num)

			# Calculate fps
			delay = time.time() - tmp_time
			tmp_time = time.time()
			fps_list.append(delay)
			if len(fps_list) > 5 * worker_num:
				fps_list.pop(0)
			fps = len(fps_list) / np.sum(fps_list)
			print('fps: {:.2f}, time: {}'.format(fps, time.ctime()))

			# Calculate frame delay, in order to make the video look smoother.
			# When fps is higher, should use a smaller ratio, or fps will be limited in a lower value.
			# Larger ratio can make the video look smoother, but fps will hard to become higher.
			# Smaller ratio can make fps higher, but the video looks not too smoother.
			# The ratios below are tested many times.
			if fps < 6:
				Global.frame_delay = (1 / fps) * 0.75
			elif fps < 20:
				Global.frame_delay = (1 / fps) * 0.5
			elif fps < 30:
				Global.frame_delay = (1 / fps) * 0.25
			else:
				Global.frame_delay = 0

			# Display the resulting image
			cv2.imshow('Video-1', rgb_write_frame_list[prev_id(Global.write_num, worker_num)])

			# show Lepton IR images
			#cv2.imshow('Video-2', write_frame_list[prev_id(Global.write_num, worker_num)])

		# Hit 'q' on the keyboard to quit!
		if cv2.waitKey(1) & 0xFF == ord('q'):
			Global.is_exit = True
			break

		time.sleep(0.01)

	# Quit
	cv2.destroyAllWindows()
	vs.stop()