# 基於 RGB-D 演算法實作員工打卡系統
## 項目說明
本專案旨在建立員工打卡系統

## 硬體要求
本次示範使用以下清單
- Pi4
- Pi Camera
- FLIR Lepton 2.5(80x60)

## 檔案結構說明
 - assets : 存放訓練用資料
 - demo : 存放 README 用檔案
 - facerec_on_camera_multiprocessing : 使用 multiprocessing 進行運算
## 使用說明
1. 下載專案
2. 打開 CML
3. cd 至專案位置
4. 執行指令 `$ python2 facerec_on_camera_multiprocessing.py`
5. 得到兩個視窗，分別名為 "Video-1"、"Video-2"，顯示 RGB、IR 影像，當人臉被觀測到時，會在畫面上顯示出名字與溫度

## 效果呈現

### [WIP]Version 0.2 : Haar Cascade + FILR Lepton 2.5 temp. measurment

### Version 0.1 : Dlib HoG + FILR Lepton 2.5 temp. measurment + 4 workers on RPI4(~2FPS, CPU only)
![Mulitprocessig_RGBD_gif](demo/multiprocessing_rgbd.gif)

## 使用函式庫
- [face_recognition](https://github.com/ageitgey/face_recognition)
- [picamera](https://picamera.readthedocs.io/en/release-1.13/install.html)
- [pylepton](https://github.com/groupgets/pylepton)
- [cv2]()
- [numpy]()
- [imutils]()
- [multiprocessing]()

## 等待增加功能
- []也許有
- []也許沒有
- []隨緣功能
